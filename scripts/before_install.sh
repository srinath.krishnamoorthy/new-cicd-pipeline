#!/bin/bash
HOME=/var/www/html
LOGFOLDER=$HOME/log
LOG=$LOGFOLDER/deploy.log
mkdir $LOGFOLDER
touch $LOG
/bin/echo "$(date '+%Y-%m-% %x'): ** Before Install Hook Started **" >> $LOG

# Do some actions before the installation
/bin/echo "$(date '+%Y-%m-% %x'): ** Before Install Hook Completed **" >> $LOG
